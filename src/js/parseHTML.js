var api = {}

function getNextPreview(data, pos)
{
	return data.indexOf('-preview-', pos)
}

function getNextTitlePos(data, pos)
{
	return data.indexOf('title=', pos)
}

function getNextTitle(data, pos)
{
	let start = getNextTitlePos(data, pos)
	while (start < data.length && data[start] !== '"')
	{
		start++
	}
	start++

	let end = start
	while (end < data.length && data[end] !== '"')
	{
		end++
	}
	return {info: data.substring(start, end), endPos: end}
}

function getURLAround(data, pos)
{
	let start =  getNextPreview(data, pos)
	if (start === -1)
	{
		return {url: "", endPos: -1}
	}
	while (start > 0 && data[start] !== '"')
	{
		start--
	}
	start++

	let end = start
	while (end < data.length && data[end] !== '"')
	{
		end++
	}

	let URLRegexExpression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi
	let URLRegex = new RegExp(URLRegexExpression)
	let URLstr = data.substring(start, end)

	if (URLstr.match(URLRegex) && URLstr.toLowerCase().endsWith(".jpg"))
	{
		return {url: URLstr, endPos: end}
	}
	else 
	{
		return getURLAround(data, end)
	}
}

function deductMP4WithJPG(url)
{
	let previewPos = getNextPreview(url, 0)
	if (previewPos === -1)
	{
		return ""
	}
	return url.substring(0, previewPos) + ".mp4"
}

api.parse = function(data)
{
	let clips = []
	let i = 0
	for (;;)
	{
		let res = getURLAround(data, i)
		if (res.endPos === -1)
		{
			break
		}
		let clip =
		{
			thumbnail: "",
			video: "",
			title: "",
			streamer: "",
			category: "",
			relativeDateOfCreation: "",
			addedTime: Date.now(),
			selected: true
		}
		clip.thumbnail = res.url
		clip.video = deductMP4WithJPG(res.url)
		res = getNextTitle(data, res.endPos)
		clip.title = res.info
		res = getNextTitle(data, res.endPos)
		clip.streamer = res.info
		res = getNextTitle(data, res.endPos)
		clip.category = res.info
		res = getNextTitle(data, res.endPos)
		clip.relativeDateOfCreation = res.info
		clips[clips.length] = clip
		if (i > res.endPos)
		{
			console.log("Error! i must only increase")
			break
		}
		i = res.endPos
	}

	let cliperNameEnd = data.indexOf(" - Clips Manager - ")
	let cliperNameStart = cliperNameEnd

	while (cliperNameStart > 0 && data[cliperNameStart] != ">")
	{
		cliperNameStart--
	}
	cliperNameStart++
	let cliperName = data.substring(cliperNameStart, cliperNameEnd)

	console.log("Parsed " + clips.length + " clips from " + cliperName)

	return {clips: clips, cliperName: cliperName}
}

api.printStat = function(db)
{

	console.log(db.clips.length + " clips")
	console.log()
	console.log("Categories:")
	for (let cat in  db.categories)
	{
		console.log(cat + ": " + db.categories[cat].clips.length + " clips.")
	}

	console.log()
	console.log("Streamers :")
	for (let s in db.streamers)
	{
		console.log(s + ": " + db.streamers[s].clips.length + " clips.")
	}
}

function getStreamers(db)
{
	let streamers = {}
	for (let i = 0; i < db.clips.length; i++)
	{
		let currStreamer = db.clips[i].streamer
		if (!streamers[currStreamer])
		{
			streamers[currStreamer] = {selected: true, clips: []}
		}
		streamers[currStreamer].clips[streamers[currStreamer].clips.length] = db.clips[i]
	}
	return streamers
}

function getCategories(db)
{
	let cats = {}
	for (let i = 0; i < db.clips.length; i++)
	{
		let currCat = db.clips[i].category
		if (!cats[currCat])
		{
			cats[currCat] = []
			cats[currCat] = {selected: true, clips: []}
		}
		cats[currCat].clips[cats[currCat].clips.length] = db.clips[i]
	}
	return cats
}

api.extendDB = function(db)
{
	db.categories = getCategories(db)
	db.streamers = getStreamers(db)
}

api.formatDB = function(db)
{
	let res = {}
	res.cliperName = db.cliperName
	res.twitterName = db.twitterName

	res.clips = []
	for (let i = 0; i < db.clips.length; i++)
	{
		if (db.clips[i].selected)
		{
			res.clips[res.clips.length] = JSON.parse(JSON.stringify(db.clips[i]))
			delete res.clips[res.clips.length-1].selected
		}
	}

	return JSON.stringify(res)
}

api.test = function(data)
{
	let db = api.parse(data)
	api.extendDB(db)
	api.printStat(db)
}

module.exports = api
