const twitchParser = require('./parseHTML')

var api = {}

var db = {}
var showClips = false
var clickedVideo = -1
var twitchName = ""
var twitterName = ""


function hash(str)
{
	var hash = 0
	if (str.length == 0) 
	{
		return hash
	}
	for (var i = 0; i < str.length; i++) 
	{
		var char = str.charCodeAt(i)
		hash = ((hash << 5) - hash) + char
		hash = hash & hash
	}
	return hash
}


function nbSelectedClips()
{
	let selected = 0
	for (let i = 0; i < db.clips.length; i++)
	{
		if (db.clips[i].selected)
		{
			selected++
		}

	}

	return selected
}

function getClipsHTML()
{
	let abc = ''

	abc += '<table>'
	for (let i = 0; i < db.clips.length; i++)
	{
		let clip = db.clips[i]

		if (clip.selected === true)
		{
			abc += '<tr id="clip' + i + '"style="background-color: #94daff">'
		}
		else
		{
			abc += '<tr  id="clip' + i + '" style="background-color: #ffffff">'
		}
		if (clickedVideo == i)
		{
			abc += '<td>' + '<video width="520" height="294" controls autoplay poster="' + clip.thumbnail + '"><source src="' + clip.video + '" type="video/mp4">video tag unsuported!</video>' + '</td>'
		}
		else
		{
			abc += '<td>' + '<img width="260" height="147" src="' + clip.thumbnail + '" onclick="javascript:window.ooc.setClickedVideo(' + i + ')">' + '</td>'
		}
		abc += '<td onclick="javascript:window.ooc.selectClip(' + i + ', '+!clip.selected +')" >' + clip.title + '</td>'
		abc += '<td onclick="javascript:window.ooc.selectClip(' + i + ', '+!clip.selected +')" >' + clip.streamer + '</td>'
		abc += '<td onclick="javascript:window.ooc.selectClip(' + i + ', '+!clip.selected +')" >' + clip.category + '</td>'
		abc += '</tr>'

	}
	abc += '</table>'
	return abc
}

function displayDB()
{

	if (document.getElementById('twitchName'))
	{
		twitchName = document.getElementById('twitchName').value
	}

	if (document.getElementById('twitterName'))
	{
		twitterName = document.getElementById('twitterName').value
	}


	let abc = ""

	abc += '<p>'
	abc += '<table>'
	abc += '<tr>'
	abc += '<td>Twitch name</td>'
	abc += '<td><input type="text" id="twitchName" value="' + twitchName + '"></td>'
	abc += '</tr>'
	abc += '<tr>'
	abc += '<td>Twitter name<br/>(optional)</td>'
	abc += '<td><input type="text" id="twitterName" value="' + twitterName + '" ></td>'
	abc += '</tr>'
	abc += '</table>'
	abc += '</p>'

	abc += '<p>'
	abc += '<input type="button" value="Send selected clip!" onclick="javascript:window.ooc.send()">'
	abc += '</p>'

	abc += '<p>'
	abc += nbSelectedClips() + "/" + db.clips.length + " clips selected.<br />"
	abc += '</p>'
	abc += "<br />"
	abc += "Categories: <br />"
	abc += "<table>"
	for (let cat in  db.categories)
	{
		if (db.categories[cat].selected === true)
		{
			abc += '<tr  id="cat' + hash(cat) + '" onclick="javascript:window.ooc.selectCat(\'' + cat + '\', false)" style="background-color: #94daff">'
		}
		else if (db.categories[cat].selected === false)
		{
			abc += '<tr  id="cat' + hash(cat) + '" onclick="javascript:window.ooc.selectCat(\'' + cat + '\', true)" style="background-color: #ff5e5e">'
		}
		else if (db.categories[cat].selected === null)
		{
			abc += '<tr  id="cat' + hash(cat) + '" onclick="javascript:window.ooc.selectCat(\'' + cat + '\', true)" style="background-color: #edc877">'
		}
		abc += "<td>" + cat + "</td><td>" + db.categories[cat].clips.length + " clips.</td></tr>"
	}
	abc += "</table>"
	abc += "<table>"

	abc += "<br />"
	abc += "Streamers :<br />"
	for (let s in db.streamers)
	{
		if (db.streamers[s].selected === true)
		{
			abc += '<tr id="s' + hash(s) + '" onclick="javascript:window.ooc.selectStreamer(\'' + s + '\', false)" style="background-color: #94daff">'
		}
		else if (db.streamers[s].selected === false)
		{
			abc += '<tr id="s' + hash(s) + '" onclick="javascript:window.ooc.selectStreamer(\'' + s + '\', true)" style="background-color: #ffffff">'
		}
		else
		{
			abc += '<tr id="s' + hash(s) + '" onclick="javascript:window.ooc.selectStreamer(\'' + s + '\', true)" style="background-color: #edc877">'
		}

		abc += "<td>" + s + "</td><td>" + db.streamers[s].clips.length + " clips.</td></tr>"
	}
	abc += "</table>"

	if (showClips)
	{
		abc += '<p><input type="button" value="Hide clips" onclick="window.ooc.toggleClipsVisibility()"></p>'
		abc += '<p>' + getClipsHTML() + '</p>'
	}
	else
	{
		abc += '<p><input type="button" value="Show clips" onclick="window.ooc.toggleClipsVisibility()"></p>'
	}


	document.getElementById('gen').innerHTML = abc

}

api.toggleClipsVisibility = function()
{
	showClips = !showClips
	displayDB()
}

function updateSelectedStreamer()
{
	for (let s in db.streamers)
	{
		let hasSelected = false
		let hasUnSelected = false
		for (let i = 0; i < db.streamers[s].clips.length; i++)
		{
			if (db.streamers[s].clips[i].selected)
			{
				hasSelected = true
			}
			else
			{
				hasUnSelected = true
			}
		}
		if (hasSelected === true && hasUnSelected === true)
		{
			db.streamers[s].selected = null
		}
		else if (hasSelected)
		{
			db.streamers[s].selected = true
		}
		else
		{
			db.streamers[s].selected = false
		}
	}
	
}

function updateSelectedCat()
{
	for (let c in db.categories)
	{
		let hasSelected = false
		let hasUnSelected = false
		for (let i = 0; i < db.categories[c].clips.length; i++)
		{
			if (db.categories[c].clips[i].selected)
			{
				hasSelected = true
			}
			else
			{
				hasUnSelected = true
			}
		}
		if (hasSelected === true && hasUnSelected === true)
		{
			db.categories[c].selected = null
		}
		else if (hasSelected)
		{
			db.categories[c].selected = true
		}
		else
		{
			db.categories[c].selected = false
		}
	}
	
}

api.selectClip = function(clipID, selected)
{
	db.clips[clipID].selected = selected

	updateSelectedStreamer()
	updateSelectedCat()

	displayDB()
}

api.selectCat = function(cat, selected)
{
	cat = cat.replace(/&/g, "&amp;")
	db.categories[cat].selected = selected
	for (let i = 0; i < db.categories[cat].clips.length; i++)
	{
		db.categories[cat].clips[i].selected = selected
	}

	updateSelectedStreamer()

	displayDB()
}

api.selectStreamer = function(s, selected)
{
	s = s.replace(/&/g, "&amp;")
	db.streamers[s].selected = selected
	for (let i = 0; i < db.streamers[s].clips.length; i++)
	{
		db.streamers[s].clips[i].selected = selected
	}

	updateSelectedCat()

	displayDB()
}

api.setClickedVideo = function(id)
{
	clickedVideo = id
	displayDB()
}

api.parseHTML = function()
{
	data = document.getElementById('twitchHTML').value
	db = twitchParser.parse(data)
	twitchParser.extendDB(db)
	twitchParser.printStat(db)
	twitchName = db.cliperName
	displayDB()

}

api.send = function()
{
	db.cliperName = twitchName
	db.twitterName = twitterName
	console.log((new Buffer(twitchParser.formatDB(db))).toString('base64'))
}

window.ooc = api

