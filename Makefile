.PHONY: all
all: js html


.PHONY: js
js: dist/index.min.js

.PHONY: html
html: dist/index.html

dist/index.min.js: $(shell find src/js/ -type f 2>/dev/null)
	@set -ex; \
	mkdir -p dist/; \
	./node_modules/browserify/bin/cmd.js src/js/index.js > dist/index.min.js

dist/index.html: $(shell find src/html -type f 2>/dev/null)
	@set -e; \
	mkdir -p dist/; \
	cp src/html/index.html dist/
